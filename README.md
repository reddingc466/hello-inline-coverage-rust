# hello-inline-coverage-rust

[![pipeline status](https://gitlab.com/fh1ch/hello-inline-coverage-rust/badges/main/pipeline.svg)](https://gitlab.com/fh1ch/hello-inline-coverage-rust/-/commits/main)

Rust example project to demonstrate the [GitLab Test Coverage Visualization feature](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html).

## Usage

Run:

```sh
cargo run
```

Test:

```sh
cargo test
```
